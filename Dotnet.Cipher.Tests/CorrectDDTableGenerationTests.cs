using System;
using Dotnet.Cipher.Implementation;
using Dotnet.Cipher.Implementation.Cryptanalysis;
using NUnit.Framework;

namespace Dotnet.Cipher.Tests
{
    [TestFixture]
    public class CorrectDDTableGenerationTests
    {

        public void GenerateDdTableTest()
        {
            var table = DifferentialAnalysis.FindDifferenceDistributionTable();

            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    Console.Write(table[i,j]);
                }
                Console.WriteLine();
            }
            var sortedTable = DifferentialAnalysis.ReduceDifferenceDistributionTable(table);

            foreach (var value in sortedTable)
            {
                Console.Write(value.ToJson());
            }
            
            Assert.Pass();
        }
    }
}
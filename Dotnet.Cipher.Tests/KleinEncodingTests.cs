using Dotnet.Cipher.Implementation.Cipher;
using NUnit.Framework;

namespace Dotnet.Cipher.Tests
{
    [TestFixture]
    public class KleinEncodingTests
    {
        private Klein _klein;
        private const int Rounds = 12;
        
        [SetUp]
        public void Setup()
        {
            _klein = new Klein(Rounds);
        }
        
        [Test]
        public void TestEncodingWithDiffKeys()
        {
            byte[] message = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

            var key1 = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            var key2 = new byte[] {0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef};
            
            byte[] expectedOne = {0xcd, 0xc0, 0xb5, 0x1f, 0x14, 0x72, 0x2b, 0xbe};
            byte[] expectedTwo = {0x59, 0x23, 0x56, 0xc4, 0x99, 0x71, 0x76, 0xc8};

            _klein.Key = key1;

            var cipherTextOne = _klein.Encrypt(message);
            
            _klein.Key = key2;

            var cipherTextTwo = _klein.Encrypt(message);
            
            Assert.AreEqual(expectedOne, cipherTextOne, "Cipher texts are not equal, but should be!");
            Assert.AreEqual(expectedTwo, cipherTextTwo, "Cipher texts are not equal, but should be!");
        }

        [Test]
        public void TestEncodingWithOnlyZeroKey()
        {
            byte[] key1 = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
            byte[] key2 = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            
            byte[] message1 = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            byte[] expected1 = {0x64, 0x56, 0x76, 0x4e, 0x86, 0x02, 0xe1, 0x54};
            
            byte[] message2 = {0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef};
            byte[] expected2 = {0x62, 0x9f, 0x9d, 0x6d, 0xff, 0x95, 0x80, 0x0e};

            _klein.Key = key1;

            var cipher1 = _klein.Encrypt(message1);
            
            _klein.Key = key2;

            var cipher2 = _klein.Encrypt(message2);
            
            Assert.AreEqual(expected1, cipher1, "Cipher texts are not equal, but should be!");
            Assert.AreEqual(expected2, cipher2, "Cipher texts are not equal, but should be!");
        }
    }
}
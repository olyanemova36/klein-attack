using Newtonsoft.Json;

namespace Dotnet.Cipher.Implementation
{
    public static class ConsoleHelper
    {
        public static string ToJson(this object @object)
        {
            return JsonConvert.SerializeObject(@object);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dotnet.Cipher.Implementation.Cipher
{
    // KLEIN-64 key setup given 64-bit key K.
    public static class KeyHelper64
    {
        private const int KEY_SEPARATOR = 4;
        private const int SECOND_BYTE = 2;
        private const int FIVE_BYTE = 5;
        private const int SIX_BYTE = 6;

        public static IEnumerable<byte> Modify(byte[] key, int round = 0)
        {
            if (round != 0)
            {
                var firstPart = Shift(key[..KEY_SEPARATOR]);
                var secondPart = Shift(key[KEY_SEPARATOR..]);
                var xorResult = Xor(firstPart, secondPart);
                
                secondPart[SECOND_BYTE] = (byte) (secondPart[SECOND_BYTE] ^ round);
                
                xorResult[FIVE_BYTE-KEY_SEPARATOR] = Klein.SBox(xorResult[FIVE_BYTE-KEY_SEPARATOR]);
                xorResult[SIX_BYTE-KEY_SEPARATOR] = Klein.SBox(xorResult[SIX_BYTE-KEY_SEPARATOR]);

                return secondPart.Concat(xorResult);
            }

            return key;
        }
        
        public static byte[] Xor(byte[] first, byte[] second)
        {
            if (IsTermsValid(first, second))
            {
                var lenght = first.Length;
                byte[] result = new byte[lenght];

                for (int i = 0; i < lenght; ++i)
                {
                    result[i] = (byte) (first[i] ^ second[i]);
                }

                return result;
            }

            throw new NotSupportedException("The method can't support \"XOR\" with not valid bytes");
        }

        private static byte[] Shift(byte[] array)
        {
            if (array != null)
            {
                byte[] copy = new byte[array.Length];
                
                copy[0] = array[1];
                copy[1] = array[2];
                copy[2] = array[3];
                copy[array.Length - 1] = array[0];

                return copy;
            }

            throw new NullReferenceException("Can't shift the null reference byte array");
        }

        private static bool IsTermsValid(params byte[][] subsequences)
        {
            var result = true;
            var commonLenght = subsequences.FirstOrDefault()?.Length;

            foreach (var subsequence in subsequences)
            {
                result &= subsequence != null && subsequence.Length == commonLenght;
            }

            return result;
        }
    }
}
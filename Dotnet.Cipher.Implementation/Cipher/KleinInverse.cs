using System;
using System.Collections.Generic;

namespace Dotnet.Cipher.Implementation.Cipher
{
    public static class KleinInverse
    {
        private const int R = 7;
        private const int PAIRS_COUNT = 7;
        private const int BIT_ENCRYPTION_DEPTH = 64;
        private const int BYTE_ENCRYPTION_DEPTH = BIT_ENCRYPTION_DEPTH / 8;

        private static byte[] _t;
        //private List<byte[]> _pairs;
        

       // public byte[] RecoverLowerNibbles()
       // {
       //     byte[] potential = CrackZeroRound(_pairs[0]); // get the first pair
        //    Console.WriteLine($"ROUND 0 KEY RECOVERED: " + BitConverter.ToString(potential));
//
        //    return potential;
      //  }

        private static bool IsCorrectPairsCount(List<byte[]> pairs)
        {
            return pairs.Count == PAIRS_COUNT;
        }

        public static byte[] CrackRound(byte[] cipher, byte[] begin)
        {
            var unmultiplyied = Helper.MixNibblesInv(cipher);
            var derotated = Helper.RotateNibblesInv(unmultiplyied);
            var unsubtituted = Klein.SubNibbles(derotated);

            Console.WriteLine("del Y " + BitConverter.ToString(cipher) + "\ndel B " + BitConverter.ToString(unsubtituted));
            
            return KeyHelper64.Xor(begin, unsubtituted);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dotnet.Cipher.Implementation.Cipher
{
    public class Klein
    {
        public List<byte[]> _sChar = new ();
        public List<byte[]> _rChar = new ();
        public List<byte[]> _mChar = new ();

        private static byte[] _key;
        private static int R = 12;
        
        private const int BIT_ENCRYPTION_DEPTH = 64;
        private const int BYTE_ENCRYPTION_DEPTH = BIT_ENCRYPTION_DEPTH / 8;
        private const int BASE_ENCRYPTION = 16;
        
        public byte[] Key
        {
            protected get => _key;
            set => _key = value;
        }

        private static byte[] _t;
        
        public Klein(int rounds) : this()
        {
            R = rounds;
        }

        public Klein()
        {
            _t = new byte[BYTE_ENCRYPTION_DEPTH];
        }

        public  byte[] Encrypt( byte[] plainText) 
        {
            if (IsMessageValid(plainText)) // only 64 bits => 2^3 -> byte => 8 bytes
            {
                var vector = CopyPlainTextToVector(plainText);
            
                vector = Process(vector); // 11 Rounds

                Key = KeyHelper64.Modify(Key, R).ToArray();
                
                //Console.WriteLine("LAST KEY XOR : " + BitConverter.ToString(KeyHelper64.Xor(vector, Key)));
                
                return KeyHelper64.Xor(vector, Key); // Last Round
            }
            throw new ArgumentException("Can't transform not valid message for cipher");
        }

        private static byte[] CopyPlainTextToVector(byte[] text)
        {
            return text;
        }

        private byte[] Process(byte[] plainText)
        {
            var result = plainText;
            
            for (int r = 0; r < R; ++r)
            {
                var begin = result;
                var xored = AddRoundKey(result, Key, r);
                var sbox = SubNibbles(xored);
                _sChar.Add(sbox);
                _t = RotateNibbles(sbox);
                _rChar.Add(_t);
                result = MixNibbles(_t);
                _mChar.Add(result);
            }
            
            return result;
        }

        public static byte SBox(byte @byte)
        {
            return (byte) (SubNibblesHigh((byte) (@byte / BASE_ENCRYPTION * BASE_ENCRYPTION)) + 
                   SubNibblesLower((byte) (@byte % BASE_ENCRYPTION)));
        }

        public static byte[] SubNibbles(byte[] plainText)
        {
            var result = new byte[BYTE_ENCRYPTION_DEPTH];
            
            for (int i = 0; i < BYTE_ENCRYPTION_DEPTH; ++i)
            {
                result[i] = SBox(plainText[i]);
            }

            return result;
        }

        private byte[] RotateNibbles(byte[] plainText)
        {
            var result = new byte[BYTE_ENCRYPTION_DEPTH];
            
            for (int i = 0; i < BYTE_ENCRYPTION_DEPTH; ++i)
            {
                result[(i + 6) % BYTE_ENCRYPTION_DEPTH] = plainText[i];
            }

            return result;
        }

        private byte[] MixNibbles(byte[] t)
        {
            var result = new byte[BYTE_ENCRYPTION_DEPTH];

            result[0] = (byte) (LFunc((byte) (t[0] ^ t[1])) ^ t[1] ^ t[2] ^ t[3]);
            result[1] = (byte) (LFunc((byte) (t[1] ^ t[2])) ^ t[0] ^ t[2] ^ t[3]);
            result[2] = (byte) (LFunc((byte) (t[2] ^ t[3])) ^ t[0] ^ t[1] ^ t[3]);
            result[3] = (byte) (LFunc((byte) (t[3] ^ t[0])) ^ t[0] ^ t[1] ^ t[2]);
            result[4] = (byte) (LFunc((byte) (t[4] ^ t[5])) ^ t[5] ^ t[6] ^ t[7]);
            result[5] = (byte) (LFunc((byte) (t[5] ^ t[6])) ^ t[4] ^ t[6] ^ t[7]);
            result[6] = (byte) (LFunc((byte) (t[6] ^ t[7])) ^ t[4] ^ t[5] ^ t[7]);
            result[7] = (byte) (LFunc((byte) (t[7] ^ t[4])) ^ t[4] ^ t[5] ^ t[6]);

            return result;
        }

        private static byte SubNibblesHigh(byte @byte) => @byte switch
        {
            0x00 => 0x70,
            0x10 => 0x40,
            0x20 => 0xa0,
            0x30 => 0x90,
            0x40 => 0x10,
            0x50 => 0xf0,
            0x60 => 0xb0,
            0x70 => 0x00,
            0x80 => 0xc0,
            0x90 => 0x30,
            0xa0 => 0x20,
            0xb0 => 0x60,
            0xc0 => 0x80,
            0xd0 => 0xe0,
            0xe0 => 0xd0,
            0xf0 => 0x50,
            _ => throw new ArgumentOutOfRangeException(nameof(@byte), @byte, null)
        };
        
        public static byte SubNibblesLower(byte @byte) => @byte switch
        {
            0x00 => 0x07,
            0x01 => 0x04,
            0x02 => 0x0a,
            0x03 => 0x09,
            0x04 => 0x01,
            0x05 => 0x0f,
            0x06 => 0x0b,
            0x07 => 0x00,
            0x08 => 0x0c,
            0x09 => 0x03,
            0x0a => 0x02,
            0x0b => 0x06,
            0x0c => 0x08,
            0x0d => 0x0e,
            0x0e => 0x0d,
            0x0f => 0x05,
            _ => throw new ArgumentOutOfRangeException(nameof(@byte), @byte, null)
        };

        private static byte LFunc(byte x) => (x & 0x80) == 0x00 ? (byte) (x << 1) : (byte) ((x << 1) ^ 0x1b);

        private bool IsMessageValid(byte[] message) => message.Length == BYTE_ENCRYPTION_DEPTH;

        private byte[] AddRoundKey(byte[] plainText, byte[] key, int round)
        {
            Key =  KeyHelper64.Modify(Key, round).ToArray();
            return KeyHelper64.Xor(plainText, Key);
        }
    }
}
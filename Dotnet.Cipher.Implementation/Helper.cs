namespace Dotnet.Cipher.Implementation
{
    public class Helper
    {
        
        private const int BIT_ENCRYPTION_DEPTH = 64;
        private const int BYTE_ENCRYPTION_DEPTH = BIT_ENCRYPTION_DEPTH / 8;
        private static byte GMul(byte a, byte b) { // Galois Field (256) Multiplication of two Bytes
            byte p = 0;

            for (int counter = 0; counter < 8; counter++) {
                if ((b & 1) != 0) {
                    p ^= a;
                }

                bool hi_bit_set = (a & 0x80) != 0;
                a <<= 1;
                if (hi_bit_set) {
                    a ^= 0x1B; /* x^8 + x^4 + x^3 + x + 1 */
                }
                b >>= 1;
            }

            return p;
        }
        
        public static byte[] MixNibbles(byte[] vec) { // 's' is the main State matrix, 'ss' is a temp matrix of the same dimensions as 's'.

            var result = new byte[4];
            
            result[0] = (byte)(GMul(0x02, vec[0]) ^ GMul(0x03, vec[1]) ^ vec[2] ^ vec[3]);
            result[1] = (byte)(vec[0] ^ GMul(0x02, vec[1]) ^ GMul(0x03, vec[2]) ^ vec[3]);
            result[2] = (byte)(vec[0] ^ vec[1] ^ GMul(0x02, vec[2]) ^ GMul(0x03, vec[3]));
            result[3] = (byte)(GMul(0x03, vec[0]) ^ vec[1] ^ vec[2] ^ GMul(0x02, vec[3]));

            return result;
        }

        public static byte[] GetVectorInv(byte[] vec) { // 's' is the main State matrix, 'ss' is a temp matrix of the same dimensions as 's'.

            var result = new byte[4];
            
            for (int c = 0; c < 4; c++) {
                result[0] = (byte)(GMul(0x0e, vec[0]) ^ GMul(0x0b, vec[1]) ^ GMul(0x0d, vec[2]) ^ GMul(0x09, vec[3]));
                result[1] = (byte)(GMul(0x09, vec[0]) ^ GMul(0x0e, vec[1]) ^ GMul(0x0b, vec[2]) ^ GMul(0x0d, vec[3]));
                result[2] = (byte)(GMul(0x0d, vec[0]) ^ GMul(0x09, vec[1]) ^ GMul(0x0e, vec[2]) ^ GMul(0x0b, vec[3]));
                result[3] = (byte)(GMul(0x0b, vec[0]) ^ GMul(0x0d, vec[1]) ^ GMul(0x09, vec[2]) ^ GMul(0x0e, vec[3]));
            }

            return result;
        }
        
        public static byte[] RotateNibblesInv(byte[] message)
        {
            var result = new byte[BYTE_ENCRYPTION_DEPTH];
            
            for (int i = 0; i < BYTE_ENCRYPTION_DEPTH; ++i)
            {
                result[i] = message[(i + 6) % BYTE_ENCRYPTION_DEPTH];
            }

            return result;
        }
        
        public static byte[] MixNibblesInv(byte[] vec) { // 's' is the main State matrix, 'ss' is a temp matrix of the same dimensions as 's'.

            var first = GetVectorInv(vec[..4]);
            var second = GetVectorInv(vec[4..]);

            var result = new byte[first.Length + second.Length];
            
            first.CopyTo(result, 0);
            second.CopyTo(result, 4);

            return result;
        }
    }
}
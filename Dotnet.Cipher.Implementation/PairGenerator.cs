using System;
using System.Collections.Generic;
using System.Linq;

namespace Dotnet.Cipher.Implementation
{
    public class PairGenerator : IDisposable
    {
        private  List<byte[]> _beginStorage;
        private  List<byte[]> _endStorage;
        private byte[] _pair;
    
        private  static readonly int BEGIN_SIZE = 2;
        private  static readonly int END_SIZE = 6;
        private  static int PAIRS_COUNT;
        private  static int CURRENT_PAIR;

        public PairGenerator(byte[] basePair, int count)
        {
            _beginStorage = new List<byte[]> { basePair[..BEGIN_SIZE] };
            _endStorage = new List<byte[]> { basePair[END_SIZE..] };
            _pair = basePair;
            PAIRS_COUNT = count;
        }

        public IEnumerable<byte[]> Generate()
        {
            while (CURRENT_PAIR != PAIRS_COUNT)
            {
                var pairFound = GeneratePair();
                CURRENT_PAIR++;
                
                yield return pairFound;
            }
        }

        private byte[] GeneratePair()
        {
            byte[] begin;
            byte[] end;
            
            using (var randomer = new CryptographyRandom())
            {
                do
                {
                    begin = randomer.GetBytes(2);
                    end = randomer.GetBytes(2);
                    
                    if (!_beginStorage.Contains(begin) && !_endStorage.Contains(end))
                    {
                        _beginStorage.Add(begin);
                        _endStorage.Add(end);

                        return begin.Concat(_pair[BEGIN_SIZE..END_SIZE]).Concat(end).ToArray();
                    }
                } 
                while (true);
            }
        }

        public void Dispose()
        {
            _beginStorage?.Clear();
            _endStorage?.Clear();

            _pair = null;
        }
    }
}
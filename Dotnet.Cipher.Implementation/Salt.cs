using System.Security.Cryptography;

namespace Dotnet.Cipher.Implementation
{
    public static class Salt
    {
        public static byte[] CreateSalt(int size)
        {
            size = 5;
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);
            return buff;
        }
    }
}
﻿using System;
using System.IO;
using System.Linq;
using Dotnet.Cipher.Implementation.Cipher;
using Dotnet.Cipher.Implementation.Cryptanalysis;
using Newtonsoft.Json;
using static System.Console;

namespace Dotnet.Cipher.Implementation
{
    class Program
    {
        static void Main(string[] args)
        {
            var klein = new Klein();

            var diffAnalys = new DifferentialAnalysis(klein);

            var charsCollection = diffAnalys.Analyse();
            
            WriteLine(charsCollection.ToJson());
            
            File.WriteAllText(@"chars.json", JsonConvert.SerializeObject(charsCollection));

           //CheckMixNibblesMatrix();
        }

        public static void CheckMixNibblesMatrix()
        {
            var higherBytes = new byte[]
            {
                0x00, 0x01, 0x02, 0x03,
                0x04, 0x05, 0x06, 0x07,
             //   0x08, 0x09, 0x0a, 0x0b,
              //  0x0c, 0x0d, 0x0e, 0x0f
            };
            
            var lowerBytes = new byte[]
            {
                0x00, 0x10, 0x20, 0x30,
                0x40, 0x50, 0x60, 0x70,
              //  0x80, 0x90, 0xa0, 0xb0,
              //  0xc0, 0xd0, 0xe0, 0xf0
            };

            var counter = 0; 
            var counterFrameHigh = 0; 
            var counterFrameLower = 0; 
            var counterOutputWithNull = 0; 

            //foreach (var byte0 in lowerBytes)
            //{
                //foreach (var byte1 in lowerBytes)
                //{
                    //foreach (var byte2 in lowerBytes)
                    //{
                        foreach (var byte3 in higherBytes)
                        {
                            WriteLine("Input:  " + BitConverter.ToString(new byte[] { 0x00, 0x00, byte3, 0x00}));

                            var output = Helper.MixNibbles(new byte[] { 0x00, 0x00,byte3,  0x00});
                            WriteLine("Output: "+ BitConverter.ToString(output));
                            counter++;
                            if (IsHighNibbleFormat(output))
                            {
                                counterFrameHigh++;
                            }
                            
                            if (IsLowerNibbleFormat(output))
                            {
                                counterFrameLower++;
                            }

                            if (IsHasNullFormat(output))
                            {
                                counterOutputWithNull++;
                            }
                        }
                   // }
              //  }
           // }

            WriteLine("Counter: " + counter);
            WriteLine("CounterFrameHigh: " + counterFrameHigh);
            WriteLine("CounterFrameLower: " + counterFrameLower);
            WriteLine("CounterOutputWithNull: " + counterOutputWithNull);
        }

        private static bool IsHighNibbleFormat(byte[] output)
        {
            return output.Aggregate(true, (current, @byte) => current & @byte is >= 16 or 0);
        }
        
        private static bool IsLowerNibbleFormat(byte[] output)
        {
            return output.Aggregate(false, (current, @byte) => current | @byte % 16 != 0);
        }
        
        private static bool IsHasNullFormat(byte[] output)
        {
            return output.Aggregate(false, (current, @byte) => current | @byte == 0);
        }
    }
}
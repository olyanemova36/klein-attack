using System;
using System.Security.Cryptography;

namespace Dotnet.Cipher.Implementation
{
    public sealed class CryptographyRandom : IDisposable
    {
        const int AES_KEY_SIZE = 16;
        private static RandomNumberGenerator _systemRandom;
        private static byte[] _tokenData = new byte[AES_KEY_SIZE];

        public byte[] GetBytes(byte[] tokenData)
        {
            _systemRandom = new RNGCryptoServiceProvider();
            _systemRandom.GetBytes(tokenData);
            return tokenData;
        }

        public byte[] GetBytes(int size)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);
            return buff;
        }

        public void Dispose()
        {
            _systemRandom?.Dispose();
        }
    }
}
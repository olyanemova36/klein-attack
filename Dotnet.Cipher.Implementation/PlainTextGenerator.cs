namespace Dotnet.Cipher.Implementation
{
    public static class PlainTextGenerator
    {
        private const int TEXT_SIZE_64 = 8;
        
        public static byte[] Generate(int index, byte @byte)
        {
            using (var random = new CryptographyRandom())
            {
                do
                {
                    var plainText = random.GetBytes(TEXT_SIZE_64);

                    if (plainText[index] == @byte)
                    {
                        return plainText;
                    }
                    
                } while (true);
            }
        }
        
        public static byte[] GenerateBasePair(byte[] plainText, int index, int diff)
        {
            using (var random = new CryptographyRandom())
            {
                do
                {
                    var pair = random.GetBytes(TEXT_SIZE_64);

                    if ((plainText[index] ^ pair[index]) == diff)
                    {
                        return pair;
                    }
                    
                } while (true);
            }
        }
    }
}
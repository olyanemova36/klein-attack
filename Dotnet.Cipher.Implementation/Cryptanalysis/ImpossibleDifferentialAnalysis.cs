using System.Collections.Generic;

namespace Dotnet.Cipher.Implementation.Cryptanalysis
{
    public class ImpossibleDifferentialsAnalysis : DifferentialAnalysis
    {
        public static List<Diff> CollectImpossibleDiffsDifferenceDistributionTable(byte[, ] table)
        {
            var tableResized = new List<Diff>();
            
            for (byte i = 0; i < SBOX_SIZE; ++i)
            {
                for (byte j = 0; j < SBOX_SIZE; ++j)
                {
                    if ((double)table[i, j]/SBOX_SIZE == MIN_PROB)
                    {
                        tableResized.Add( new Diff(i, j,(double)table[i, j]/SBOX_SIZE));
                    }
                }
            }
            
            return tableResized;
        }
    }
}
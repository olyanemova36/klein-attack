namespace Dotnet.Cipher.Implementation.Cryptanalysis
{
    public class Diff
    {
        public byte Dx { get; set; }
        public byte Dy { get; set; }
        public double Probability { get; set; }

        public Diff(byte dx, byte dy, double prob)
        {
            Dx = dx;
            Dy = dy;
            Probability = prob;
        }
    }
}
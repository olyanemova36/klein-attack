using System.Collections.Generic;

namespace Dotnet.Cipher.Implementation.Cryptanalysis
{
    public class Step
    {
        public List<RaechedSbox> To { get; set; }
        
        public Diff Path { get; set; }

        public Step(Diff path, List<RaechedSbox> to)
        {
            To = to;
            Path = path;
        }
    }
}
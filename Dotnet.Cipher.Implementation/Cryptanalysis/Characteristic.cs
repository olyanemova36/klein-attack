using System.Collections.Generic;

namespace Dotnet.Cipher.Implementation.Cryptanalysis
{
    public class Characteristic
    {
        public StartPoint Start { get; set; }
        public List<Diff> Probabilities { get; set; }
        public List<RaechedSbox> State { get; set;}
    }
}
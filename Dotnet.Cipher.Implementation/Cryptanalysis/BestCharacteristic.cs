using System.Collections.Generic;

namespace Dotnet.Cipher.Implementation.Cryptanalysis
{
    public class BestCharacteristic
    {
        public byte Dx { get; set; }
        public double Probability { get; set; }
        
        public List<RaechedSbox> SBoxes { get; set; }
    }
}
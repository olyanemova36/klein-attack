using System;
using System.Collections.Generic;
using System.Linq;
using Dotnet.Cipher.Implementation.Cipher;

namespace Dotnet.Cipher.Implementation.Cryptanalysis
{
    public class DifferentialAnalysis // 2^64 pairs of plain text
    {
        private Klein _klein;

        private static byte[,] DDTTable { get; set; } = new byte[SBOX_SIZE, SBOX_SIZE];
        private byte[] _plainDiffs;
        private byte[] _secondRoundDiffs;
        
        // ===== CONSTANTS =====
        private const int BYTE_BLOCK_SIZE = 8;
        protected const int SBOX_SIZE = BYTE_BLOCK_SIZE * 2;
        protected const double MIN_PROB = 0.001;
        private const int NUM_ROUNDS = 3;
        private const int NUM_SBOXES = 16;

        public DifferentialAnalysis(Klein klein)
        {
            _klein = klein;
        }

        protected DifferentialAnalysis() { }

        public List<BestCharacteristic> Analyse()
        {
            // analyse the S-BOX and creates the DDT-Table
            var table = FindDifferenceDistributionTable();
            var sortedTable = ReduceDifferenceDistributionTable(table)
                .OrderBy(x => x.Probability)
                .Where(x => x.Dy != 16)
                .ToList();
            
            // calculate all possible differential characteristics
            var diffCharacteristics = GetDiffCharacteristics(sortedTable);
            var sortedDiffCharacteristics = CustomSort(diffCharacteristics);

            return sortedDiffCharacteristics;
        }

        // calculate all the possible differential characteristics given the table
        // recoursive function
        public List<Characteristic> GetDiffCharacteristics(
            List<Diff> advancedDDTable, 
            List<Characteristic> currentState = null, 
            int depth = 1)
        {
            // run for NUM_ROUNDS - 1
            
            if (depth == NUM_ROUNDS)
            {
                if (currentState.Count == 0)
                {
                    throw new Exception(
                        "No differential characteristic found! May be MIN_PROB is too high or MAX_BLOCKS_TO_BF too low.");
                }

                return currentState;
            }
            
            // at the beginnig, only one sbox can be chosen
            // (this could be done differently)
            if (depth == 1)
            {
                // for each bias and each sbox, calculate which sboxes are reached (in the lower layer)
                // this will be the next step's new initial state
                var current = new List<Characteristic>();
                
                foreach (var diff in advancedDDTable)
                {
                    for (int numSBox = 0; numSBox < NUM_SBOXES; ++numSBox)
                    {
                        var characteristic = new Characteristic();

                        characteristic.Probabilities = new List<Diff>{diff} ;
                        characteristic.Start = new StartPoint(depth, numSBox);
                        characteristic.State = GetDestination(numSBox, diff.Dy);
                        
                        current.Add(characteristic);
                    }
                }

                return GetDiffCharacteristics(advancedDDTable, current, depth + 1);
            }
            else
            {
                var nextState = new List<Characteristic>();

                // calculate all possible moves from 'curr_sbox'
                var totalCombinations = 1;
                var startSboxes = new RaechedSbox[NUM_SBOXES];
                var possibleStepPerSbox = new Dictionary<RaechedSbox, List<Step>>();
                var numPossibleStepPerSbox = new Dictionary<RaechedSbox, int>();
                var iStartSboxes = 0;

                foreach (var state in currentState)
                {
                    var currentPosition = state.State;

                    foreach (var currSBox in currentPosition)
                    {
                        var y = currSBox.InputSBoxByte;
                        
                        var possibleSteps = new List<Step>();
                        var possibleDiffs = advancedDDTable.Where(diff => diff.Dx == y).ToList();
                        
                        foreach (var diff in possibleDiffs)
                        {
                            var sboxReached = GetDestination(currSBox.SBoxIndex, diff.Dy);

                            var step = new Step(diff, sboxReached);
                            possibleSteps.Add(step);
                        }

                        if (possibleSteps.Count > 0)
                        {
                            totalCombinations *= possibleSteps.Count;
                            possibleStepPerSbox[currSBox] = possibleSteps;
                            startSboxes[iStartSboxes] = currSBox;
                            numPossibleStepPerSbox[currSBox] = possibleSteps.Count;
                            ++iStartSboxes;
                        }
                    }

                    if (totalCombinations == 0)
                    {
                        continue;
                    }

                    var possibleStepsCombination = new List<List<Step>>();

                    for (int i = 0; i < totalCombinations; ++i)
                    {
                        var newComb = new List<Step>();
                        newComb.Add(
                            possibleStepPerSbox[startSboxes[0]][i % numPossibleStepPerSbox[startSboxes[0]]]);

                        foreach (var sbox in startSboxes)
                        {
                            if (sbox.SBoxIndex == 0) continue;
                            var mod = 1;

                            for (int prevSbox = 0; prevSbox < sbox.SBoxIndex; ++prevSbox)
                            {
                                mod *= numPossibleStepPerSbox[startSboxes[prevSbox]];
                            }

                            var index = (i / mod) % numPossibleStepPerSbox[sbox];
                            newComb.Add(possibleStepPerSbox[sbox][index]);
                        }
                        possibleStepsCombination.Add(newComb);
                    }

                    foreach (var possibleSteps in possibleStepsCombination)
                    {
                        var characteristic = new Characteristic();

                        characteristic.Probabilities = state.Probabilities; // TODO: Add replacement constructor
                        characteristic.Start = state.Start;
                        characteristic.State = new List<RaechedSbox>();

                        foreach (var step in possibleSteps)
                        {
                            characteristic.Probabilities.Add(step.Path);

                            foreach (var reachedSbox in step.To)
                            {
                                if (!characteristic.State.Contains(reachedSbox))
                                {
                                    characteristic.State.Add(reachedSbox);
                                }

                                var newBits = reachedSbox.InputSBoxByte;
                                characteristic.State.Find(x => x.SBoxIndex == reachedSbox.SBoxIndex).InputSBoxByte 
                                 = reachedSbox.InputSBoxByte;
                            }
                        }

                        var probs = characteristic.Probabilities;
                        double resultProb = 1.0;
                        foreach (var diff in probs)
                        {
                            resultProb *= diff.Probability;
                        }
                        resultProb *= 100;

                        if (resultProb >= MIN_PROB)
                        {
                            nextState.Add(characteristic);
                        }
                    }
                }
                return GetDiffCharacteristics(advancedDDTable, nextState, depth + 1);
            }
        }

        private static List<BestCharacteristic> CustomSort( List<Characteristic> diffChars)
        {
            var sortedDiffs = new List<BestCharacteristic>();
            foreach (var diffChar in diffChars)
            {
                var probabilities = diffChar.Probabilities;
                var resultPropability = 1.0;

                foreach (var diff in probabilities)
                {
                    resultPropability *= diff.Probability;
                }
                resultPropability *= 100;

                var bestDiff = probabilities[0];
                var numSBox = diffChar.Start;

                var entry = new BestCharacteristic()
                    {
                        Dx = bestDiff.Dx, 
                        Probability = resultPropability, 
                        SBoxes = diffChar.State
                    };
                if (resultPropability > MIN_PROB)
                {
                    sortedDiffs.Add(entry);
                }
            }
            return sortedDiffs
                .OrderByDescending(@char => @char.Probability)
                .Where(@char => @char.Probability != 100.0)
                .ToList();
        }

        public List<RaechedSbox> GetDestination(int numSBox, byte dy)
        {
            // do the permutation and mix nibble step 
            var reachedSBoxes = new List<RaechedSbox>();
            var newSbox = (numSBox + 12) % NUM_SBOXES;

            var diffVector = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            diffVector[GetByte(newSbox)] = dy;

            if (newSbox % 8 == 1)
            {
                var reachedValue = GetSpreadByteArray(Helper.MixNibbles(diffVector[..4]));
                for (int i = 0; i < 8; i++)
                {
                    reachedSBoxes.Add(new RaechedSbox {SBoxIndex = i, InputSBoxByte = reachedValue[i]});
                }
            }
            else
            {
                var reachedValue = GetSpreadByteArray(Helper.MixNibbles(diffVector[4..]));
                for (int i = 8; i < 16; i++)
                {
                    reachedSBoxes.Add(new RaechedSbox {SBoxIndex = i, InputSBoxByte = reachedValue[i%8]});
                }
            }
            
            Console.WriteLine(reachedSBoxes.ToJson());
            
            return reachedSBoxes;
        }

        private static int GetByte(int position) => position switch
        {
            0 => 0,
            1 => 0,

            2 => 1,
            3 => 1,

            4 => 2,
            5 => 2,

            6 => 3,
            7 => 3,

            8 => 4,
            9 => 4,

            10 => 5,
            11 => 5,

            12 => 6,
            13 => 6,

            14 => 7,
            15 => 7,
            _ => 0
        };

        private static byte[] GetSpreadByteArray(byte[] array )
        {
            var result = new List<byte>(array.Length * 2);
            foreach (var @byte in array)
            {
                result.Add((byte)(@byte & 0xF0));
                result.Add((byte)(@byte & 0x0F));
            }

            return result.ToArray();
        }

        // creates the DD-Table for corresponding S-Box
        public static byte[,] FindDifferenceDistributionTable()
        {
            var table = new byte[SBOX_SIZE, SBOX_SIZE];
            for (byte x_1 = 0; x_1 < SBOX_SIZE; ++x_1)
            {
                for (byte x_2 = 0; x_2 < SBOX_SIZE; ++x_2)
                {
                    var y_1 = Klein.SubNibblesLower(x_1);
                    var y_2 = Klein.SubNibblesLower(x_2);
                    
                    table[x_1 ^ x_2, y_1 ^ y_2]++;
                }
            }
            return table;
        }
        
        // only keeps the dx and dy pairs that have more than zero hits
        public static List<Diff> ReduceDifferenceDistributionTable(byte[, ] table)
        {
            var tableResized = new List<Diff>();
            
            for (byte i = 0; i < SBOX_SIZE; ++i)
            {
                for (byte j = 0; j < SBOX_SIZE; ++j)
                {
                    if ((double)table[i, j]/SBOX_SIZE >= MIN_PROB)
                    {
                        tableResized.Add( new Diff(i, j,(double)table[i, j]/SBOX_SIZE));
                    }
                }
            }
            
            return tableResized;
        }

        public static byte FindLowerKey(byte input1, byte input2, byte diff)
        {
            var lowerInput1 = input1 & 0x0f;
            var lowerInput2 = input2 & 0x0f;
            var lowerDiff  = diff & 0x0f;
            
            for (byte i = 0; i < 0x0f; i++)
            {
                if ((Klein.SubNibblesLower((byte) (lowerInput1 ^ i)) ^
                    Klein.SubNibblesLower((byte) (lowerInput2 ^ i))) == 
                    lowerDiff)
                {
                    Console.WriteLine(i);
                }
            }

            return 1;
        }
    }
}
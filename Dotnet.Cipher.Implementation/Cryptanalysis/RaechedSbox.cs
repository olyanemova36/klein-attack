using System.Collections.Generic;
using System.Dynamic;

namespace Dotnet.Cipher.Implementation.Cryptanalysis
{
    public class RaechedSbox
    {
        public int SBoxIndex { get; set; }

        public byte InputSBoxByte { get; set; }
    }
}
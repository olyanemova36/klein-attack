namespace Dotnet.Cipher.Implementation.Cryptanalysis
{
    public class StartPoint
    {
        public int Depth { get; set; }
        public int SBoxIndex { get; set; }

        public StartPoint(int depth, int sbox)
        {
            Depth = depth;
            SBoxIndex = sbox;
        }
    }
}